package xyz.sachonidas.chc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class Main2Activity extends AppCompatActivity {

    Spinner sp1;
    EditText etPeso, etHoras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);



        sp1 = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.hidratos_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp1.setAdapter(adapter);

        etPeso = (EditText)findViewById(R.id.etPeso);
        etHoras = (EditText)findViewById(R.id.etHoras);



    }

    public void onTrash(View view){
        etPeso.setText("");
        etHoras.setText("");
        sp1.setSelection(0);
    }

}
